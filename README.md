# Dracula desktop

Première tentative de ricing Debian avec le thème Dracula et i3wm.

![Bureau vide, i3wm et dracula](empty_desktop.png)

<br />

![Bureau avec fenêtres d'ouvertes, i3wm et dracula](windows_tiling.png)

## Sommaire
- [Dracula desktop](#dracula-desktop)
  - [Sommaire](#sommaire)
  - [Avant-propos](#avant-propos)
    - [Qu'est-ce que le *"ricing"* ?](#quest-ce-que-le-ricing-)
    - [Dracula ?](#dracula-)
  - [Ricing](#ricing)
    - [Debian : Display manager \&  Desktop environment](#debian--display-manager---desktop-environment)
    - [Installation du thème GTK](#installation-du-thème-gtk)
    - [Installation du thème QT5](#installation-du-thème-qt5)
    - [Police d'écriture](#police-décriture)
    - [GRUB](#grub)
    - [LightDM](#lightdm)
    - [i3](#i3)
      - [Couleurs](#couleurs)
      - [i3lock-color](#i3lock-color)
      - [Transition douce](#transition-douce)
      - [Barre d'état : i3blocks](#barre-détat--i3blocks)
      - [Power Menu](#power-menu)
    - [Explorateur de fichiers](#explorateur-de-fichiers)
    - [KeepassXC](#keepassxc)
    - [Terminal](#terminal)
    - [Polikit](#polikit)
    - [Feh](#feh)
    - [GIMP](#gimp)
  - [Annexe](#annexe)
    - [Firmwares propriétaires AMD](#firmwares-propriétaires-amd)
    - [Multi-écrans](#multi-écrans)
    - [Connexion internet](#connexion-internet)
    - [Importer une configuration OpenVPN et la lancer au démarrage](#importer-une-configuration-openvpn-et-la-lancer-au-démarrage)
    - [Activer par défaut la touche NumLock](#activer-par-défaut-la-touche-numlock)
    - [Création d'un server SSH](#création-dun-server-ssh)
    - [Autres outils installés Dracula compatible](#autres-outils-installés-dracula-compatible)
  - [Sources](#sources)

## Avant-propos

> **Important :** Je n'ai fait que centraliser les ressources d'autres personnes, n'hésitez pas à aller consulter leur travail, les sources sont indiquées

### Qu'est-ce que le *"ricing"* ?

*"Rice" est un mot couramment utilisé pour faire référence aux améliorations et personnalisations visuelles de son bureau. Il a été hérité de la pratique consistant à personnaliser des voitures d'importation asiatiques bon marché pour les faire paraître plus rapides qu'elles ne l'étaient en réalité - ce qui était également connu sous le nom de "ricing" [...] Il est utilisé pour faire référence à un bureau visuellement attrayant amélioré au-delà de la configuration par défaut.* <sup>1</sup>

### Dracula ?

Le thème Dracula fait référence à une palette de couleur dans les tons violets <sup>2</sup> :

<table class="wikitable mw-datatable">
<caption>Dracula Color Palette
</caption>
<tbody><tr>
<th>Name
</th>
<th>Swatch
</th>
<th>Hex
</th>
<th>RGB
</th>
<th>HSL
</th></tr>
<tr>
<td>Background
</td>
<td title="color 282A36" style="background:  #282a36; color:  ##000; text-align: auto;">
</td>
<td><samp>#282a36</samp>
</td>
<td align="center">40 42 54
</td>
<td align="center">231° 15% 18%
</td></tr>
<tr>
<td>Current Line
</td>
<td title="color 44475A" style="background:  #44475a; color:  ##000; text-align: auto;">
</td>
<td><samp>#44475a</samp>
</td>
<td align="center">68 71 90
</td>
<td align="center">232° 14% 31%
</td></tr>
<tr>
<td>Foreground
</td>
<td title="color F8F8F2" style="background:  #f8f8f2; color:  ##000; text-align: auto;">
</td>
<td><samp>#f8f8f2</samp>
</td>
<td align="center">248 248 242
</td>
<td align="center">60° 30% 96%
</td></tr>
<tr>
<td>Comment
</td>
<td title="color 6272A4" style="background:  #6272a4; color:  ##000; text-align: auto;">
</td>
<td><samp>#6272a4</samp>
</td>
<td align="center">98 114 164
</td>
<td align="center">225° 27% 51%
</td></tr>
<tr>
<td>Cyan
</td>
<td title="color 8BE9FD" style="background:  #8be9fd; color:  ##000; text-align: auto;">
</td>
<td><samp>#8be9fd</samp>
</td>
<td align="center">139 233 253
</td>
<td align="center">191° 97% 77%
</td></tr>
<tr>
<td>Green
</td>
<td title="color 50FA7B" style="background:  #50fa7b; color:  ##000; text-align: auto;">
</td>
<td><samp>#50fa7b</samp>
</td>
<td align="center">80 250 123
</td>
<td align="center">135° 94% 65%
</td></tr>
<tr>
<td>Orange
</td>
<td title="color FFB86C" style="background:  #ffb86c; color:  ##000; text-align: auto;">
</td>
<td><samp>#ffb86c</samp>
</td>
<td align="center">255 184 108
</td>
<td align="center">31° 100% 71%
</td></tr>
<tr>
<td>Pink
</td>
<td title="color FF79C6" style="background:  #ff79c6; color:  ##000; text-align: auto;">
</td>
<td><samp>#ff79c6</samp>
</td>
<td align="center">255 121 198
</td>
<td align="center">326° 100% 74%
</td></tr>
<tr>
<td>Purple
</td>
<td title="color BD93F9" style="background:  #bd93f9; color:  ##000; text-align: auto;">
</td>
<td><samp>#bd93f9</samp>
</td>
<td align="center">189 147 249
</td>
<td align="center">265° 89% 78%
</td></tr>
<tr>
<td>Red
</td>
<td title="color FF5555" style="background:  #ff5555; color:  ##000; text-align: auto;">
</td>
<td><samp>#ff5555</samp>
</td>
<td align="center">255 85 85
</td>
<td align="center">0° 100% 67%
</td></tr>
<tr>
<td>Yellow
</td>
<td title="color F1FA8C" style="background:  #f1fa8c; color:  ##000; text-align: auto;">
</td>
<td><samp>#f1fa8c</samp>
</td>
<td align="center">241 250 140
</td>
<td align="center">65° 92% 76%
</td></tr></tbody></table>

Aujourd'hui, le theme s'intègre à 257 applications <sup>3</sup> et le dépôt github totalise plus de 18 000 étoiles. <sup>4</sup>

L'objectif de ce dépôt est de permettre de configurer une distribution Debian 11 fraichement installée pour la vêtir des plus belles couleurs de Dracula dans son intégralité.

## Ricing

### Debian : Display manager &  Desktop environment

A l'issue de l'installation de Debian, si un environnement de bureau tel que *Gnome* ou *KDE* n'a pas été sélectionné, on arrive sur une interface en ligne de commande. 

L'objectif va donc être d'installer le gestionnaire de session [LightDM](https://wiki.debian.org/fr/LightDM), puis le gestionnaire de bureau [i3wm](https://i3wm.org/) qui sera notre environnement graphique.

```
sudo apt install lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
```

```
sudo apt install i3
```

### Installation du thème GTK

1. Téléchargez le thème depuis le site officiel : https://draculatheme.com/gtk
2. Déplacez le dans `/usr/share/themes/`
3. Téléchargez le pack d'icônes associés : https://github.com/m4thewz/dracula-icons
3. Déplacez le dans `/usr/share/icons`
4. Installez [LXAppearance](https://wiki.lxde.org/fr/LXAppearance) :
   ```
   sudo apt install lxappearance
   ```
5. Appliquez le thème et le pack d'icônes depuis `lxappearance`, ou saisissez les commandes suivantes :
   ```
   gsettings set org.gnome.desktop.interface gtk-theme "Dracula"
   gsettings set org.gnome.desktop.wm.preferences theme "Dracula"
   gsettings set org.gnome.desktop.interface icon-theme "Dracula"
   ```
6. Vous pouvez vérifier que les thèmes sont bien appliqués dans les fichiers suivants :
   ```
   ~/.gtkrc-2.0
   ~/.config/gtk-3.0/settings.ini
   ```

### Installation du thème QT5

Pour certaines applications telles que *KeepassXC* par exemple, l'application du [thème Dracula sur QT5](https://draculatheme.com/qt5) va être nécessaire.

Pour ce faire, 
- Téléchargez le fichier `Dracula.conf` depuis le lien du paragraphe précédent
- Copiez le fichier dans `~/.config/qt5ct/colors/`
- Modifiez le fichier de configuration pour qu'il ressemble à ceci :
  ```
  [Appearance]
  color_scheme_path=/home/motoko/.config/qt5ct/colors/Dracula.conf
  ...
  ```
- Vérifiez l'application du thème grâce à l'outil `qt5ct`

![](2022-04-03-09-46-32.png)

### Police d'écriture

La police utilisée est la [Yosemite San Francisco Font](https://github.com/supermarin/YosemiteSanFranciscoFont).

Une fois téléchargés, les fichiers peuvent être déplacés dans `~/.fonts/`, et appliqués à l'aide de `lxappearance`

> Note : Il est possible que la police n'apparaisse pas dans `lxappearance`. Dans ce cas :
> - Modifiez la ligne `gtk-font-name` dans `~/.gtkrc-2.0` comme ceci :
>   ```
>   gtk-font-name="System San Francisco Display 10"
>   ```
> - Faites de même dans `~/.config/gtk-3.0/settings.ini` :
>   ```
>   gtk-font-name=System San Francisco Display 10
>   ```
> La police devrait alors apparaitre dans le menu déroulant du gestionaire de thème


### GRUB

1. Téléchargez le thème : https://draculatheme.com/grub
2. Copiez le thème dans `/boot/grub/themes`
3. Utilisez `grub-customizer` pour appliquer le thème
![](2022-04-22-17-20-07.png)

### LightDM

LightDM utilise les thèmes GTK, le gros du travail a déjà été fait puisqu'il suffit d'appliquer le thème Dracula et le pack d'icones précédemment placés dans `/usr/share/themes` et `/usr/share/icons`.

Les fichiers de configurations se trouvent dans `/etc/lightdm`, on peut cependant utiliser le paquet `lightdm-gtk-greeter-settings` pour une application graphique.

Pour changer le moniteur sur lequel doit apparaitre par défaut la fenêtre de connexion, on pourra utiliser la variable `active-monitor`.

```
cat /etc/lightdm/lightdm-gtk-greeter.conf

[greeter]
background = /home/motoko/Pictures/debian.png
theme-name = gtk-master
icon-theme-name = Dracula
default-user-image = /home/motoko/Pictures/003-broom.svg
indicators = ~clock;~spacer;~language;~session;~a11y;~power
active-monitor=0
```

### i3

Toutes les modifications se trouvent dans le fichier `config`, initialement dans `~/.config/i3/config`. N'hésitez pas à regarder la page officielle en complément : https://draculatheme.com/i3

#### Couleurs

```
# Dracula Color Palette :
# Background 		   #282a36 	40 42 54 	   231° 15% 18%
# Current Line 		#44475a 	68 71 90 	   232° 14% 31%
# Foreground 		   #f8f8f2 	248 248 242 	60° 30% 96%
# Comment 		      #6272a4 	98 114 164 	   225° 27% 51%
# Cyan 		         #8be9fd 	139 233 253 	191° 97% 77%
# Green 		         #50fa7b 	80 250 123 	   135° 94% 65%
# Orange 		      #ffb86c 	255 184 108 	31° 100% 71%
# Pink 		         #ff79c6 	255 121 198 	326° 100% 74%
# Purple 		      #bd93f9 	189 147 249 	265° 89% 78%
# Red 		        #ff5555 	255 85 85 	   0° 100% 67%
# Yellow 		     #f1fa8c 	241 250 140 	65° 92% 76%

set $bg-color            #282a36
set $inactive-bg-color   #282a36
set $text-color          #f8f8f2
set $inactive-text-color #676e7d
set $urgent-bg-color     #ff5555

# window colors
#                       border              background         text                 indicator
client.focused          $bg-color           $bg-color          $text-color          #50fa7b
client.unfocused        $inactive-bg-color  $inactive-bg-color $inactive-text-color #50fa7b
client.focused_inactive $inactive-bg-color  $inactive-bg-color $inactive-text-color #50fa7b
client.urgent           $urgent-bg-color    $urgent-bg-color   $text-color          #50fa7b
# Indicator is the green bar at the edge of the window which informs where the next window will appear.
# It can be disabled with the line below :
hide_edge_borders both

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
  	status_command i3blocks -c ~/.config/i3/i3blocks.conf
	colors {
		background $bg-color
	    	separator #757575
		#                  border             background         text
		focused_workspace  $bg-color          $bg-color          $text-color
		inactive_workspace $inactive-bg-color $inactive-bg-color $inactive-text-color
		urgent_workspace   $urgent-bg-color   $urgent-bg-color   $text-color
	}
}
```

#### i3lock-color

> Note : Un dépôt officiel Dracula a été mis à disposition, n'hésitez pas à y faire un tour à l'adresse https://github.com/dracula/i3lock-color

La source des commandes ci-dessous sont récupérées du dépôt non-officiel suivant : https://github.com/Raymo111/i3lock-color

1. Pour Debian 11, installez les dépendances
   ```
   sudo apt install autoconf gcc make pkg-config libpam0g-dev libcairo2-dev libfontconfig1-dev libxcb-composite0-dev libev-dev libx11-xcb-dev libxcb-xkb-dev libxcb-xinerama0-dev libxcb-randr0-dev libxcb-image0-dev libxcb-util0-dev libxcb-xrm-dev libxkbcommon-dev libxkbcommon-x11-dev libjpeg-dev
   ```
2. Clonez le dépôt git :
   ```
   git clone https://github.com/Raymo111/i3lock-color.git
   ```
3. Installez i3lock-color
   ```
   cd i3lock-color
   ./install-i3lock-color.sh
   ```
4. Adaptez la configuration d'i3 dans `~/.config/i3/config` pour y faire apparaitre les nouvelles commandes :
   ```
   bindsym $mod+Shift+x exec i3lock --color '#000000' --ring-color='#282a36' --line-color='#282a36' --keyhl-color='#8be9fd' --insidever-color='#8be9fd' --ringver-color='#76c2d3' --insidewrong-color='#ff5555' --ringwrong-color='#ff5555'
   ```



#### Transition douce

Une transition douce à l'ouverture des fenêtres ou au changement d'espace de travail peut être obtenu grâce au paquet *compton* :

```
sudo apt install compton
cat ~/.config/compton.conf 
# Fade
fading = true;
fade-delta = 7.0;
fade-in-step = 0.03;
fade-out-step = 0.028;
alpha-step = 0.06;
```

#### Barre d'état : i3blocks

La barre d'état native d'i3wm peut être améliorée grâce à [i3blocks](https://github.com/vivien/i3blocks)

Pour cela, sous Debian 11, il suffit d'installer les paquets `i3blocks` et  `libnotify-bin`, puis de taper la commande suivante pour appliquer la configuration par défaut : `cp /etc/i3blocks.conf ~/.config/i3/i3blocks.conf`

Plus qu'à personnaliser, le fichier `i3blocks.conf` est présent à la racine du dépôt git.

![](2022-04-22-17-26-16.png)

#### Power Menu

Voir https://github.com/jluttine/rofi-power-menu

Lors d'une installation fraîche d'i3wm, il n'y a pas de power menu, autrement dit il faut passer par un terminal pour éteindre le PC. On va donc mettre en place un menu à l'aide de *Rofi*

1. Installez le paquet avec `sudo apt install rofi`
2. Configurer rofi (voir le fichier `rofi-power-menu` à la racine du dépôt git)
3. Mettre Rofi au theme dracula :
   ```
   curl https://raw.githubusercontent.com/dracula/rofi/master/theme/config1.rasi -o ~/.config/rofi/config.rasi
   ```
4. Rajouter la ligne suivante dans la config d'i3 pour associer une combinaison de touches au clavier : 
   ```
   bindsym $mod+p exec rofi -show power-menu -modi "power-menu:~/.config/i3/rofi-power-menu --choices=shutdown/reboot/hibernate/suspend/logout"
   ```

### Explorateur de fichiers

L'explorateur [Thunar](https://fr.wikipedia.org/wiki/Thunar) a une excellente intégration du thème GTK Dracula (paquet `thunar`).

Pour le sélectionner par défaut à la place de *Nautilus* par exemple, tapez la commande suivante :
```
xdg-mime default Thunar-folder-handler.desktop inode/directory
```
Ou modifiez le fichier `~/.local/share/applications/mimeapps.list` pour y rajouter `Thunar-folder-handler.desktop`. 

On peut voir les modifications appliquées en faisant un clic droit sur un dossier, puis *Propriétés* pour voir avec quel logiciel il s'ouvre par défaut (donc normalement *Thunar*)

### KeepassXC

L'[installation du thème GTK](#installation-du-thème-qt5) est un prérequis.

Une fois installé, il suffit d'ouvrir KeepassXC, se rendre dans `Affichage > Thème`, et selectionner *Classique* pour obtenir le rendu suivant :

![](2022-04-03-10-04-57.png)


### Terminal

![](2022-04-22-17-25-54.png)

Le terminal utilisé est le paquet *Konsole* sous *ZSH* avec le module *Bullet *train*.

Une fois ces paquets installés, le schéma de couleur peut se télécharger [ici](https://draculatheme.com/konsole)

Il suffit ensuite de le déposer dans `~/.local/share/konsole`, puis aller dans *Konsole > Settings > Edit Current Profile… > Appearance tab* pour sélectionner *Dracula* depuis le panneau *Color Schemes & Background…*

### Polikit

Pour exécuter certaines applications, il peut être nécessaire d'avoir un agent polkit.
Un agent d'authentification polkit est utilisé pour s'assurer que l'utilisateur de la session soit bien l'utilisateur authentifié.

Par défaut, il n'est pas présent sur i3wm. On peut donc en installer [un](https://wiki.archlinux.org/title/Polkit#Authentication_agents), ici le paquet `lxqt-policykit` qui s'adaptera parfaitement à notre thème Dracula grâce à son interface Qt.

On démarrera ensuite notre agent au démarrage de la session en rajoutant cette ligne à notre fichier de configuration i3 :

```
exec --no-startup-id lxqt-policykit-agent
```

### Feh

Les fonds d'écrans seront gérés avec le paquet `feh`. Après installation, on pourra personnaliser les bureaux avec la commande suivante intégrée au fichier de configuration i3

```
feh --bg-scale /home/motoko/Pictures/debian.png --bg-fill /home/motoko/Pictures/debian.png
```

### GIMP

1. Installez GIMP
   ```
   sudo apt install gimp
   ```
2. [Téléchargez](https://github.com/dracula/gimp/archive/master.zip) le thème, puis décompressez le dossier `Dracula`
3. Copiez le thème dans le bon répertoire
   ```
   cp -r ~/gimp/Dracula/ ~/.config/GIMP/2.10/themes/
   ```
4. Lancez GIMP, et activez le thème dans *Edit > Preferences > Interface > Theme > Dracula : OK*

## Annexe

### Firmwares propriétaires AMD

Lors d'une fresh install, l'un de mes deux moniteurs n'était pas fonctionnel. Le problème a été réglé en :

1. Modifiant le fichier `/etc/apt/sources.list` pour rajouter les paquets **contrib** et **non-free** comme ceci :
   ```
   deb http://ftp.fr.debian.org/debian/ bullseye main contrib non-free
   deb-src http://ftp.fr.debian.org/debian/ bullseye main contrib non-free
   ```

### Multi-écrans

Si vous avez plusieurs écrans, leur configuration peut-être délicate avec l'outil `xrandr`. Une interface graphique peut-être apportée avec le paquet `arandr` :

![](arandr.png)

Cet outil a notamment l'avantage de fournir la ligne de commande `xrandr` utilisée dans certaines configuration, notamment ici dans le fichier de configuration d'i3.

Dans mon cas, pour un setup double écran full-hd avec un écran vertical à gauche, l'export est le suivant :

```sh
#!/bin/sh
xrandr --output DisplayPort-1 --off --output DisplayPort-0 --off --output DisplayPort-2 --mode 1920x1080 --pos 1080x488 --rotate normal --output DVI-D-0 --off --output HDMI-A-0 --mode 1920x1080 --pos 0x0 --rotate left
```

### Connexion internet

Lors de la précédente *fresh-install* de Debian 11, j'ai remarqué que la configuration de mon interface ethernet était tombée... (à la suite de l'installation des firmwares amd ?).

Pour réobtenir la connexion, j'ai installé le paquet `resolvconf`, puis configuré le fichier `/etc/network/interfaces` de la manière suivante :

```
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

auto enp4s0
#iface enp4s0 inet dhcp
iface enp4s0 inet static
	address 192.168.1.37
	network 192.168.1.0
	netmask 255.255.255.0
	# Freebox address
	gateway 192.168.1.254
	# Quad9 and OpenDNS nameservers
	dns-nameserver 9.9.9.9
	dns-nameserver 208.67.222.222
```
J'ai ensuite relancé l'interface, et obtenu la connexion à internet.

```
sudo ifdown enp4s0 && sudo ifdown enp4s0
```

### Importer une configuration OpenVPN et la lancer au démarrage

1. Installez le paquet suivant :
   ```
   sudo apt-get install openvpn
   ```
2. Copiez la configuration téléchargée sur la plateforme de votre fournisseur VPN dans le dossier `/etc/openvpn`, et renommez là `openvpn.conf`
   ```
   sudo cp ~/Downloads/<config>.ovpn /etc/openvpn/openvpn.conf
   ```
3. Créez un fichier `auth.txt` dans le répertoire précédent, contenant en première ligne l'identifiant de la connexion, et en seconde ligne le mot de passe.
4. Changez les droits de ce fichier, ainsi que son propriétaire.
   ```
   sudo chmod 400 /etc/openvpn/auth.txt
   sudo chown root:root /etc/openvpn/auth.txt
   ```
5. Activez le lancement du service au démarrage de la session linux :
   ```
   sudo systemctl enable openvpn
   ```
6. Redémarrez votre ordinateur, et vérifiez que votre IP traverse bien le VPN (https://ipleak.net/). Si ça ne fonctionne pas, consultez les logs openvpn dans `/var/log/syslog`

> Il est possible que l'IPv6 ne soit pas prise en charge par votre VPN. Dans ce cas, vous pouvez la désactiver via la commande `nmtui`, en éditant la connection que vous utilisez, puis en sélectionnant `IPv6 CONFIGURATION <Disabled>`.

### Activer par défaut la touche NumLock

1. Installez le paquet suivant :
   ```
   sudo apt install numlockx
   ```
2. Éditez le fichier de configuration de LightDM :
   ```
   sudo gedit /etc/lightdm/lightdm.conf
   ```
3. Rajoutez cette ligne à la fin du fichier :
   ```
   greeter-setup-script=/usr/bin/numlockx on
   ```

### Création d'un server SSH

1. Installation :
   ```
   sudo apt install openssh-server xauth
   ```
2. Configuration :
   ```
   grep "^[^#*/;]" /etc/ssh/sshd_config
   ```
   ```
   Include /etc/ssh/sshd_config.d/*.conf
   Port <à changer>
   PermitRootLogin No
   MaxAuthTries 3
   MaxSessions 2
   PubkeyAuthentication yes
   PasswordAuthentication no
   ChallengeResponseAuthentication no
   UsePAM yes
   X11Forwarding yes
   X11UseLocalhost no
   PrintMotd no
   Banner none
   AcceptEnv LANG LC_*
   Subsystem       sftp    /usr/lib/openssh/sftp-server
   ```
3. Redémarrez le service SSH :
   ```
   sudo systemctl restart sshd  
   ```
3. Génération de la paire de clé sur la machine client :
   ```
   ssh-keygen -o -t rsa -a 100 -b 8192 -f id_rsa-8192 -C <commentaire>
   ```
4. Envoi de la clé au serveur depuis la machine client:
   ```
   ssh-copy-id -i <nom_cle>.pub <user>@<adresse>
   ```
5. Activation de la redirection des ports sur la Box internet du FAI en cas d'accès hors du réseau interne
6. Connexion:
   ```
   ssh -Y -X -i <private_key> <user>@<address>
   ```

### Autres outils installés Dracula compatible

- **Anki**, programme d'aide à la mémorisation
- **Calibre**, bibliothèque numérique
- **Cherrytree**, application de prise de note avec organisation hiérarchique
- **Chromium**, navigateur open source basé sur la navigateur Chrome
- **Codium**, éditeur de code
   ```
   sudo apt install snapd
   sudo snap install core
   sudo snap install codium --classic
   # Pour avoir codium dans le PATH pour i3
   sudo ln -s /snap/bin/codium /usr/local/bin/codium
   ```
- **FFMPEG**, solution cross-platform pour enregistrer, convertir et diffuser de l'audio/vidéo
- **Flameshot**, outil complet de capture d'écran
- **Geeqie**, visionneuse d'image
- **Gnome Disk Utility**, paquet permettant de réaliser plusieurs opérations sur les disques de stockages / partitions
- **KDEConnect**, pour synchroniser son smartphone avec son PC
- **KDEnlive**, pouir réaliser du montage vidéo
- **LMMS**, logiciel libre de production musicale pouvant servir de séquenceur et de synthétiseur
- **NCDU**, paquet d'aide à l'évaluation de la consommation d'espace disque en ligne de commande
- **Nextcloud-Desktop**, client de synchronisation pour la solution Nextcloud (contrairement à l'AppImage, le paquet Debian fonctionne à merveille). En cas de problème pour accorder l'accès à l'application de bureau (bouton *"Grant Access"* qui boucle), faites un clic-droit > Inspecter l'élément, puis sur le lien post en HTTP > clic droit Edit as HTML, et rajouter un S pour faire un transfert en https.
- **Reshift**, filtre anti lumière bleue
- **RSync**, pour synchroniser des dossiers et réaliser des sauvegardes
- **Signal**, client de bureau pour la messagerie sécurisée
- **Translate Shell**, traducteur en ligne de commande qui combiné avec `xsel` permet de traduire du texte sélectionné (voir *[How to translate any selected text in your favourite language on Linux Mint](https://ideneal.medium.com/translate-selected-text-eeb6801aca4f)*)
  ```bash
  #!/usr/bin/env bash
  #translate-select.sh
  notify-send --icon=info "$(xsel -o)" "$(xsel -o | trans -b :fr)"
  ```
- **Transmission**, client pour les transferts p2p
- **Veracrypt**, solution de chiffrement de fichiers à la volée
- **VLC Media Player**, lecteur multimedia
- **Xournal++**, application de prise de note manuscrite

## Sources
- <sup>1</sup> r/unixporn - the home for *NIX customization! : https://www.reddit.com/r/unixporn/wiki/themeing/dictionary#wiki_rice
- <sup>2</sup> Dracula (color scheme), Wikipedia : https://en.wikipedia.org/wiki/Dracula_(color_scheme)
- <sup>3</sup> Dracula : https://draculatheme.com/
- <sup>4</sup> Github - Dracula theme : https://github.com/dracula/dracula-theme/stargazers
